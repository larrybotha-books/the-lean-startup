# The Lean Startup

```mermaid
mermaid

```
```mermaid
graph TD;
    Learn-->Ideas;
        Ideas-->Build;
            Build-->Product;
                Product-->Measure
                    Measure-->Data
                        Data->Learn;
                        
```
